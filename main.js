// ==UserScript==
// @name         AutoTrimpsV2+unimod
// @version      2.1.5.6-genbtc-8-26-2017+UniMod
// @description  try to take over the world!
// @author       zininzinin, spindrjr, belaith, ishakaru, genBTC, Unihedron
// @include      *trimps.github.io*
// @include      *kongregate.com/games/GreenSatellite/trimps
// @grant        none
// ==/UserScript==

/* THIS IS THE CODE TO GET MY SHIT TO WORK

var script = document.createElement('script');
script.id = 'AutoTrimp-script';
script.src = 'https://gl.githack.com/teh1person0/TrimpRunner/raw/master/main.js';
document.head.appendChild(script);
*/

//Add Jquery to my Project.

var script = document.createElement('script');
script.src = 'https://code.jquery.com/jquery-3.1.0.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);

// This is where my code starts!

var run = true;
var shouldGather = true;

function generateUI () {
    //Need to add a UI that will let me change things up mid run without having to reload the page.

    $('#settingsRow tbody tr').append('<td id="cSetBut">Cooper Settings</td>');

    $('#cSetBut').click(function() {
        $('#settingsHere').toggle();
		//$('#settingTitleBar').toggle();
        $('#settingSearchResults').empty();
        $('#settingSearchResults').append('<div id="runCheck" class="optionContainer"><div class="settingsBtn settingBtn1">Toggle Running</div></div>');
        $('#settingSearchResults').append('<div id="gatherCheck" class="optionContainer"><div class="settingsBtn settingBtn1">Toggle Auto Gather</div></div>');

        $('#runCheck').click(function() {
            run = !run;
            if (run) {
                $('#runCheck .settingsBtn').removeClass('settingBtn2');
                $('#runCheck .settingsBtn').addClass('settingBtn1');
            } else {
                $('#runCheck .settingsBtn').removeClass('settingBtn1');
                $('#runCheck .settingsBtn').addClass('settingBtn2');
            }
        })

        $('#gatherCheck').click(function() {
            shouldGather = !shouldGather;
            if (run) {
                $('#gatherCheck .settingsBtn').removeClass('settingBtn2');
                $('#gatherCheck .settingsBtn').addClass('settingBtn1');
            } else {
                $('#gatherCheck .settingsBtn').removeClass('settingBtn1');
                $('#gatherCheck .settingsBtn').addClass('settingBtn2');
            }
        })
    })
}

generateUI();

function gather() {
    //Logic to decide what my thing should be doing.

    //Check Begining of game starts
    if(game.resources.food === 0) {
        return 'food';
    }

    if(game.resources.food['owned'] >= 10 && game.resources.metal['owned'] === 0) {
        return 'metal';
    }

    //Currently I have no reason not to fight whenever possible.

    //if I have enough traps to get trimps to fight then I should.

    //Order occording to tony: Building (Other than Traps), Checking Traps (if Needed), Science (If upgrades that need science), Metal (If I have a Turkimp), Food, Wood.
    // Not sure how I will know to switch between Food and Wood.

    //Need to have an override button.

    return 'food'
}

setInterval(function() { if (run) {

if (shouldGather) { setGather(gather()); }

if (game.global.buildingsQueue.length === 0) {
    buyBuilding('Trap');
}

var unempTrimps = parseInt($('#jobsTitleUnemployed').text().split(" ")[0]);
var totalWorkers = unempTrimps + parseInt($('#FarmerOwned').text()) + parseInt($('#LumberjackOwned').text()) + parseInt($('#MinerOwned').text()) + parseInt($('#ScientistOwned').text()) + parseInt($('#TrainerOwned').text() + parseInt($('#ExplorerOwned').text()));



var jobs = [['Farmer',0.3],['Lumberjack',0.3],['Miner',0.3],['Scientist',0.03],['Trainer',0.05],['Explorer',0.02]];



var totalWorkers = 0;

jobs.forEach(function(job) {
    totalWorkers += game.jobs[job[0]].owned;
})

console.log(totalWorkers);

jobs.forEach(function(job) {
    unempTrimps = game.workspaces;
    if (unempTrimps > 0) {
    //console.log(job);
        if (game.jobs[job[0]].owned < job[1]*totalWorkers) {
            buyJob(job[0])
            console.log('Bought a '+job[0]);
        }
    } else if (game.jobs[job[0]].owned > job[1]*totalWorkers*1.05) {
            console.log('Too many '+job[0]);
            //fireMode();
            //buyJob(job[0]);
            //fireMode();
        }
});

if ($('#Resort').hasClass("thingColorCanAfford")) {
    console.log('Building Hotel');
    buyBuilding('Resort');
}

if ($('#Hotel').hasClass("thingColorCanAfford")) {
    console.log('Building Hotel');
    buyBuilding('Hotel');
}

if ($('#Mansion').hasClass("thingColorCanAfford")) {
    console.log('Building Mansion');
    buyBuilding('Mansion');
}

if ($('#House').hasClass("thingColorCanAfford")) {
    console.log('Building House');
    buyBuilding('House');
}

if ($('#Hut').hasClass("thingColorCanAfford")) {
    //console.log('Building Hut');
    //buyBuilding('Hut');
}

if (game.resources.food.owned > game.resources.food.max*.9 ) {
    console.log('Building Barn');
    buyBuilding('Barn');
}

if (game.resources.wood.owned > game.resources.wood.max*.9 ) {
    console.log('Building Shed');
    buyBuilding('Shed');
}

if (game.resources.metal.owned > game.resources.metal.max*.9 ) {
    console.log('Building Forge');
    buyBuilding('Forge');
}

//console.log(game.equipment);

for (var test in game.equipment) {
    var item = game.equipment[test];
    if(item.attack) {
        var costper = getBuildingItemPrice(game.equipment[test],"metal",true,1) / item.attackCalculated;
        //console.log(item, costper);
        $('#'+test+' .thingName').text(test+' ' + Math.floor(costper).toPrecision(3));
    } else if (item.health) {
        if (game.equipment[test].cost.wood) {
            var costper = getBuildingItemPrice(game.equipment[test],"wood",true,1) / item.healthCalculated;
            $('#'+test+' .thingName').text(test +' '+ Math.floor(costper).toPrecision(3));
        } else {
            var costper = getBuildingItemPrice(game.equipment[test],"metal",true,1) / item.healthCalculated;
            $('#'+test+' .thingName').text(test+' ' + Math.floor(costper).toPrecision(3));
        }
    } else {

    }
}

}},1000);
